package com.afs.unittest;

import com.afs.unittest.Project.Project;
import com.afs.unittest.Project.ProjectType;
import com.afs.unittest.expense.ExpenseType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExpenseServiceTest {
    private Object internalProject;

    @Test
    void should_return_internal_expense_type_when_getExpenseCodeByProject_given_internal_project() {
        // given
        Project internalProject = new Project(ProjectType.INTERNAL, "internalProject");
        ExpenseService expenseService = new ExpenseService();

        // when
        ExpenseType expenseCodeByProject = expenseService.getExpenseCodeByProject(internalProject);

        // then
        assertEquals(1,2);
        assertEquals(ExpenseType.INTERNAL_PROJECT_EXPENSE, expenseCodeByProject);

    }

    @Test
    void should_return_expense_type_A_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_A() {
        // given


        // when

        // then

    }

    @Test
    void should_return_expense_type_B_when_getExpenseCodeByProject_given_project_is_external_and_name_is_project_B() {
        // given

        // when

        // then

    }

    @Test
    void should_return_other_expense_type_when_getExpenseCodeByProject_given_project_is_external_and_has_other_name() {
        // given

        // when

        // then

    }

    @Test
    void should_throw_unexpected_project_exception_when_getExpenseCodeByProject_given_project_is_invalid() {
        // given

        // when
        // then

    }
}